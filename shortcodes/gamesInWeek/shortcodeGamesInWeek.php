<?php
/**
 * User: domin
 * Date: 23.05.2018
 * Time: 23:37
 */

/**
 * @param $atts
 * @return string
 */
function gamesInWeek_func($atts)
{
    $a = shortcode_atts(array(
        'week' => 'last',
        'spieltag' => false
    ), $atts);
    $week = $a['week'];

    $gamesInWeek = new GamesInWeek($week);
    $gamesInWeekLayout = new GamesInWeekLayout($gamesInWeek);
    $gamesInWeekTabelle = $gamesInWeekLayout->buildLayout($a['spieltag']);
    return $gamesInWeekTabelle;
}


