<?php
/**
 * User: domin
 * Date: 23.05.2018
 * Time: 22:47
 */

final class GamesInWeekLayout
{
    private $gamesInWeek;

    /**
     * GamesInWeekLayout constructor.
     * @param GamesInWeek $gamesInWeek
     */
    final public function __construct(GamesInWeek $gamesInWeek)
    {
        $this->gamesInWeek = $gamesInWeek;
    }

    /**
     * @param bool $forMatchday
     * @return string
     */
    final public function buildLayout(bool $forMatchday)
    {
        $gamesInWeekChart = $this->getGamesInWeekChart($forMatchday);
        return $gamesInWeekChart;
    }

    /**
     * @param bool $forMatchday
     * @return string
     */
    final private function getGamesInWeekChart(bool $forMatchday)
    {
        if ($this->gamesInWeek->hasGames()) {
            $chartFactory = new ChartFactory();
            $rowFactory = new RowFactory();
            $table = $chartFactory->getChartGamesInWeek($this->gamesInWeek->getWeek(), $forMatchday);
            foreach ($this->gamesInWeek->getAllGamesInWeek() as $key => $value) {
                $row = $rowFactory->getGamesRow();

                $classScore = [];
                if ($this->gameIsWon($value)) {
                    $classScore[] = 'victory';
                }
                $classHomeTeam = [];
                if($this->isWeinstadt($value['gHomeTeam'])){
                    $classHomeTeam[] = 'weinstadt';
                }
                $classGuestTeam = [];
                if($this->isWeinstadt($value['gGuestTeam'])){
                    $classGuestTeam[] = 'weinstadt';
                }

                if ($value['gHomeGoals'] <> '-' && $value['gGuestGoals'] <> '-') {
                    $classScore[] = 'score';
                    $row->addItem('<span class="score">' . $value['gHomeGoals'] . ' - ' . $value['gGuestGoals'] . '</span>', 'Dat/Erg', $classScore);
                } else {
                    list($day, $month) = explode(".", $value['gDate']);
                    $row->addItem($day . '.' . $month . '. ' . $value['gTime'], 'Dat/Erg', []);
                }

                $row->addItem($value['liga'], 'Liga', []);
                $row->addItem($value['gHomeTeam'], 'Heim', $classHomeTeam);
                $row->addItem($value['gGuestTeam'], 'Gast', $classGuestTeam);

                $table->addRow(clone $row);
            }
            return $table->getTable();
        } else {
            return '';
        }
    }

    /**
     * @return array
     */
    final private function fetchIds()
    {
        $idStringJSON = file_get_contents(HVW_SPIELDATEN_ROOT_DIR . 'data/ids.json');
        return json_decode($idStringJSON, true);
    }

    private function gameIsWon(array $gamedata)
    {
        if ($this->isWeinstadt($gamedata['gHomeTeam'])) {
            if ($gamedata['gHomeGoals'] > $gamedata['gGuestGoals']) {
                return true;
            }
        } elseif ($this->isWeinstadt($gamedata['gGuestTeam'])) {
            if ($gamedata['gGuestGoals'] > $gamedata['gHomeGoals']) {
                return true;
            }
        }
        return false;
    }

    private function isWeinstadt($teamName)
    {
        $idObject = $this->fetchIds();
        foreach ($idObject as $team => $teamData) {
            if ($teamName == $team) {
                return true;
            }
        }
        return false;
    }
}