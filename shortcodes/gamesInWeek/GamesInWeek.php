<?php

final class GamesInWeek
{
    private $allGamesInWeek = [];
    private $week;

    /**
     * gamesInWeek constructor.
     * @param string $week
     */
    final public function __construct(string $week)
    {
        $this->week = $week;
        if ($week == 'next') {
            $startDate = $this->getTargetDay(-1);
        } else {
            $startDate = $this->getTargetDay(-2);
        }

        $ids = $this->fetchIds();
        $gamesInWeek = $this->fetchGamesForIds($ids, $startDate);
        $this->allGamesInWeek = $this->sortGames($gamesInWeek, 'gDate');
    }

    /**
     * @param int $weekDifference
     * @return false|int
     */
    final private function getTargetDay(int $weekDifference)
    {
        $DAY = 'Monday';
        return strtotime($weekDifference . ' ' . $DAY);
    }

    /**
     * @return array
     */
    final private function fetchIds()
    {
        $idStringJSON = file_get_contents(HVW_SPIELDATEN_ROOT_DIR . 'data/ids.json');
        return json_decode($idStringJSON, true);
    }

    /**
     * @param array $idObject
     * @param string $startDate
     * @return array
     */
    final private function fetchGamesForIds(array $idObject, string $startDate)
    {
        $allGamesInWeek = [];
        foreach ($idObject as $team => $liga) {
            foreach ($liga as $ligaName => $ids) {
                try {
                    $gamesJSON = file_get_contents(HVW_SPIELDATEN_ROOT_DIR . 'data/' . $ids['Mannschaft'] . '.json');
                    $gamesObj = json_decode($gamesJSON, true);
                } catch (Exception $exception) {
                    $gamesObj = array();
                }
                $gamesOfWeek = $this->fetchGamesOfWeek($startDate, $gamesObj);
                if (count($gamesOfWeek) > 0) {
                    foreach ($gamesOfWeek as $game) {
                        $game['liga'] = explode('-',$ligaName,2)[1];
                        $allGamesInWeek[count($allGamesInWeek)] = $game;
                    }
                }
            }
        }
        return $allGamesInWeek;
    }

    /**
     * @param int $date
     * @param array $gamesObj
     * @return array
     */
    final private function fetchGamesOfWeek(int $date, $gamesObj)
    {
        $gamesInWeekOfTeam = [];

        foreach ($gamesObj as $game) {
            $gameDate = strtotime($this->normalizeStringDate($game['gDate']));
            if ($gameDate > $date && $gameDate < ($date + (60 * 60 * 24 * 7))) {
                $gamesInWeekOfTeam[count($gamesInWeekOfTeam)] = $game;
            }
        }
        return $gamesInWeekOfTeam;
    }


    private function normalizeStringDate($stringDate)
    {
        //dateformat: dd.mm.yyyy  [01.09.2017]
        $dateArray = explode('.', $stringDate);
        return $dateArray[0] . '.' . $dateArray[1] . '.20' . $dateArray[2];
    }

    private function sortGames($unsortedGames, $order = SORT_ASC)
    {
        $sortablePastGames = array();
        $sortableFutureGames = array();
        $sortedGames = array();

        $pastGames = $this->getGames($unsortedGames, 'past');
        $futureGames = $this->getGames($unsortedGames, 'future');

        foreach ($pastGames as $gameKey => $gameData) {
            $teamName = $this->getTeamName($gameData);
            $sortablePastGames[$gameKey] = $teamName;
        }
        asort($sortablePastGames);

        foreach ($futureGames as $gameKey => $gameData) {
            $normalizedTime = str_replace(':', '', $gameData['gTime']);
            $unixDate = strtotime($this->normalizeStringDate($gameData['gDate']) . ' ' . $normalizedTime);
            $sortableFutureGames[$gameKey] = $unixDate;
        }
        asort($sortableFutureGames);

        $i = 0;
        foreach ($sortablePastGames as $k => $v) {
            $sortedGames[$i] = $unsortedGames[$k];
            $i++;
        }
        foreach ($sortableFutureGames as $k => $v) {
            $sortedGames[$i] = $unsortedGames[$k];
            $i++;
        }
        return $sortedGames;
    }

    /**
     * @param array $games
     * @param string $time
     *
     * @return array
     */
    private function getGames($games, $time)
    {
        $pastGames = array();
        $futureGames = array();

        foreach ($games as $gameKey => $gameData) {
            $normalizedTime = str_replace(':', '', $gameData['gTime']);
            $unixDate = strtotime($this->normalizeStringDate($gameData['gDate']) . ' ' . $normalizedTime);
            if ($unixDate < time()) {
                $pastGames[$gameKey] = $gameData;
            } else {
                $futureGames[$gameKey] = $gameData;
            }
        }

        if ($time == 'past') {
            return $pastGames;
        } else if ($time == 'future') {
            return $futureGames;
        } else {
            return array();
        }
    }

    /**
     * @param array $gameData
     * @return string
     */
    private function getTeamName($gameData)
    {
        $teamName = '';
        $idObject = $this->fetchIds();
        foreach ($idObject as $team => $teamData) {
            if ($gameData['gHomeTeam'] == $team || $gameData['gGuestTeam'] == $team) {
                $teamName = $team;
                break;
            }
        }

        return $teamName;
    }


    /**
     * @return string
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * @return array
     */
    public function getAllGamesInWeek()
    {
        return $this->allGamesInWeek;
    }

    /**
     * @return bool
     */
    public function hasGames()
    {
        return !empty($this->allGamesInWeek);
    }
}