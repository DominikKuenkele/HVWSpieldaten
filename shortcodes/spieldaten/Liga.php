<?php
/**
 * User: domin
 * Date: 23.05.2018
 * Time: 22:23
 */

final class Liga
{
    private $name;
    private $ligaId;
    private $mannschatsId;

    private $games;
    private $table;

    /**
     * Liga constructor.
     * @param string $name
     * @param int $ligaId
     * @param int $mannschatsId
     */
    public function __construct(string $name, int $ligaId, int $mannschatsId)
    {
        $this->name = $name;
        $this->ligaId = $ligaId;
        $this->mannschatsId = $mannschatsId;

        $this->fetchGames();
        $this->fetchTable();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * @return array
     */
    public function getTable()
    {
        return $this->table;
    }

    private function fetchGames()
    {
        try {
            $gameJSON = file_get_contents(HVW_SPIELDATEN_ROOT_DIR . 'data/' . $this->mannschatsId . '.json');
            $this->games = json_decode($gameJSON, JSON_PRETTY_PRINT);
        } catch (Exception $exception) {
            $this->games = array();
        }
    }

    private function fetchTable()
    {
        try {
            $tableJSON = file_get_contents(HVW_SPIELDATEN_ROOT_DIR . 'data/' . $this->ligaId . '.json');
            $this->table = json_decode($tableJSON, JSON_PRETTY_PRINT);
        } catch (Exception $exception) {
            $this->games = array();
        }
    }
}