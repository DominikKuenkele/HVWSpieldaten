<?php
/**
 * User: domin
 * Date: 23.05.2018
 * Time: 22:12
 */

final class MannschaftsDaten
{
    /**
     * @var string
     */
    private $teamName;

    /**
     * @var Liga[]
     */
    private $ligen = array();

    /**
     * MannschaftsDaten constructor.
     * @param string $teamName
     */
    final public function __construct(string $teamName)
    {
        $this->teamName = $teamName;
        $this->fetchIds();
    }

    final private function fetchIds()
    {
        $idStringJSON = file_get_contents(HVW_SPIELDATEN_ROOT_DIR . 'data/ids.json');
        $idStringObj = json_decode($idStringJSON, true);


        if ($this->teamName == 'all') {
            foreach ($idStringObj as $team) {
                foreach ($team as $ligaName => $ids) {
                    $this->ligen[] = new Liga($ligaName, $ids["Liga"], $ids["Mannschaft"]);
                }
            }
        }else{
            foreach ($idStringObj[$this->teamName] as $ligaName => $ids) {
                $this->ligen[] = new Liga($ligaName, $ids["Liga"], $ids["Mannschaft"]);
            }
        }
    }

    /**
     * @return Liga[]
     */
    final public function getLigen()
    {
        return $this->ligen;
    }

    /**
     * @return string
     */
    final public function getTeamname()
    {
        return $this->teamName;
    }
}