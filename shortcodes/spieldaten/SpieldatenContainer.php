<?php
/**
 * User: domin
 * Date: 24.05.2018
 * Time: 18:49
 */

final class SpieldatenContainer
{
    private $header = '';
    private $link = '';
    private $content = '';

    /**
     * @param string $content
     */
    final public function setContent(string $content)
    {
        if ($content != null && $content != '') {
            $this->content = $content;
        }
    }

    /**
     * @param string $headerText
     * @param string $id
     */
    final public function setHeader(string $headerText, string $id)
    {
        $idText = '';
        if ($id != null && $id != '') {
            $idText = ' id = "' . $id . '"';
        }
        if ($headerText != null && $headerText != '') {
            $this->header = '<h4' . $idText . '>' . $headerText . '</h4>';
        }
    }

    /**
     * @param string $text
     * @param string $link
     */
    final public function setLink(string $text, string $link)
    {
        if ($text != null && $text != '') {
            if ($link != null && $text != '') {
                $this->link = '<a href="' . $link . '">' . $text . '</a>';
            }
        }
    }

    /**
     * @return string
     */
    final public function getContainer()
    {
        if ($this->content <> '') {
            return $this->header . $this->link . $this->content;
        }
        return '';
    }

    /**
     * @return string
     */
    final public function getMatchdayContainer()
    {
        return $this->content;
    }
}