<?php
/**
 * User: domin
 * Date: 23.05.2018
 * Time: 22:47
 */

include_once('MannschaftsDaten.php');
include_once(HVW_SPIELDATEN_ROOT_DIR . 'html_view/TabContainer.php');

final class SpieldatenLayout
{
    /**
     * @var MannschaftsDaten
     */
    private $mannschaftsdaten;
    /**
     * @var Liga[]
     */
    private $ligen;

    /**
     * SpieldatenLayout constructor.
     * @param MannschaftsDaten $mannschaftsDaten
     */
    final public function __construct(MannschaftsDaten $mannschaftsDaten)
    {
        $this->mannschaftsdaten = $mannschaftsDaten;
        $this->ligen = $mannschaftsDaten->getLigen();
    }

    /**
     * @param string $doSpieldaten
     * @param string $tableSize
     * @param bool $forMatchday
     * @return string
     */
    final public function buildLayout(string $doSpieldaten, string $tableSize, bool $forMatchday)
    {
        $result = '';
        $isTabContainer = false;
        $tabContainer = new TabContainer();

        foreach ($this->ligen as $liga) {

            $gamesChart = null;
            $tableChart = null;

            if ($doSpieldaten <> 'false') {
                $gamesChart = $this->getGamesChart($liga->getName(), $liga->getGames(), $forMatchday);
            }
            if ($tableSize == 'long' || $tableSize == 'short') {
                $tableChart = $this->getTableChart($liga->getName(), $liga->getTable(), $tableSize, $forMatchday);
            }

            $tableContainer = new SpieldatenContainer();
            $gamesContainer = new SpieldatenContainer();
            if ($tableChart <> null && $tableChart->getTable() <> '') {
                $id = $tableChart->getId();
                $tableContainer->setHeader('Tabelle', $id);
                $tableContainer->setContent($tableChart->getTable());
                $gamesContainer->setLink('zur Tabelle', '#' . $id);
            }

            if ($gamesChart <> null && $gamesChart->getTable() <> '') {
                $id = $gamesChart->getId();
                $gamesContainer->setHeader('Spiele', $id);
                $gamesContainer->setContent($gamesChart->getTable());
                $tableContainer->setLink('zur den Spielen', '#' . $id);
            }

            if (!$forMatchday){
                $tabContent = $tableContainer->getContainer() . $gamesContainer->getContainer();
                if ($tabContent <> '') {
                    $tabContainer->addTab($liga->getName(), $tabContent);
                }
                $isTabContainer = true;
            }else{
                $result .= $tableContainer->getMatchdayContainer() . '<div class="tableSeparator"></div>';
                $result .= $gamesContainer->getMatchdayContainer() . '<div class="tableSeparator"></div>';
            }

        }
        if($isTabContainer){
            return do_shortcode($tabContainer->getTabContainer());
        }else{
            return $result;
        }
    }

    /**
     * @param string $ligaName
     * @param array $games
     * @param bool $forMatchday
     * @return Chart
     */
    final private function getGamesChart(string $ligaName, array $games, bool $forMatchday)
    {
        $chartFactory = new ChartFactory();
        $rowFactory = new RowFactory();
        $table = $chartFactory->getChartGames($ligaName, $forMatchday);
        foreach ($games as $key => $value) {
            $row = $rowFactory->getGamesRow();

            $classScore = [];
            if ($this->gameIsWon($value)) {
                $classScore[0] = 'victory';
            }
            $classHomeTeam = [];
            if($this->isWeinstadt($value['gHomeTeam'])){
                $classHomeTeam[0] = 'weinstadt';
            }
            $classGuestTeam = [];
            if($this->isWeinstadt($value['gGuestTeam'])){
                $classGuestTeam[0] = 'weinstadt';
            }

            if ($value['gHomeGoals'] <> '-' && $value['gGuestGoals'] <> '-'){
                $row->addItem('<span class="score">' . $value['gHomeGoals'] . ' - ' . $value['gGuestGoals'] . '</span>', 'Dat/Erg', $classScore);
            }else{
                list($day, $month) = explode(".", $value['gDate']);
                $row->addItem($day . '.' . $month . '. ' . $value['gTime'], 'Dat/Erg', []);
            }
            $row->addItem($value['gHomeTeam'], 'Heim', $classHomeTeam);
            $row->addItem($value['gGuestTeam'], 'Gast', $classGuestTeam);

            $table->addRow(clone $row);
        }
        return $table;
    }

    /**
     * @param string $ligaName
     * @param array $table
     * @param string $tableSize
     * @param bool $forMatchday
     * @return Chart
     */
    final private function getTableChart(string $ligaName, array $table, string $tableSize, bool $forMatchday)
    {
        $chartFactory = new ChartFactory();
        $rowFactory = new RowFactory();
        if ($tableSize == 'short') {
            $tableChart = $chartFactory->getShortChartTable($ligaName, $forMatchday);

            foreach ($table as $rowNumber => $rowData) {
                $row = $rowFactory->getShortTableRow();
                $weinstadtClass = '';

                if ($this->containsWeinstadt($rowData['tabTeamname'])) {
                    $weinstadtClass = ' weinstadt';
                }
                $row->addItem($rowData['tabScore'], '', [$weinstadtClass]);
                $row->addItem($rowData['tabTeamname'], 'Team', [$weinstadtClass]);
                $row->addItem($rowData['pointsPlus'] . ':' . $rowData['pointsMinus'], 'Punkte', [$weinstadtClass]);

                $tableChart->addRow(clone $row);
            }
        } else {
            $tableChart = $chartFactory->getLongChartTable($ligaName, $forMatchday);

            foreach ($table as $rowNumber => $rowData) {
                $row = $rowFactory->getLongTableRow();
                $weinstadtClass = '';

                if ($this->containsWeinstadt($rowData['tabTeamname'])) {
                    $weinstadtClass = ' weinstadt';
                }
                $row->addItem($rowData['tabScore'], '', [$weinstadtClass]);
                $row->addItem($rowData['tabTeamname'], 'Team', [$weinstadtClass]);
                $row->addItem($rowData['numPlayedGames'], 'Sp', [$weinstadtClass]);
                $row->addItem($rowData['numWonGames'], 'S', [$weinstadtClass]);
                $row->addItem($rowData['numEqualGames'], 'U', [$weinstadtClass]);
                $row->addItem($rowData['numLostGames'], 'N', [$weinstadtClass]);
                $row->addItem($rowData['numGoalsShot'] . ':' . $rowData['numGoalsGot'], 'Tore', [$weinstadtClass]);
                $row->addItem($rowData['pointsPlus'] . ':' . $rowData['pointsMinus'], 'Punkte', [$weinstadtClass]);

                $tableChart->addRow(clone $row);
            }
        }
        return $tableChart;
    }

    /**
     * @param string $text
     * @return bool
     */
    final private function containsWeinstadt(string $text)
    {
        if (strpos($text, 'SG Weinstadt') !== false) {
            return true;
        } else {
            return false;
        }
    }

    final private function fetchIds()
    {
        $idStringJSON = file_get_contents(HVW_SPIELDATEN_ROOT_DIR . 'data/ids.json');
        return json_decode($idStringJSON, true);
    }

    private function gameIsWon(array $gamedata)
    {
        if ($this->isWeinstadt($gamedata['gHomeTeam'])) {
            if ($gamedata['gHomeGoals'] > $gamedata['gGuestGoals']) {
                return true;
            }
        } elseif ($this->isWeinstadt($gamedata['gGuestTeam'])) {
            if ($gamedata['gGuestGoals'] > $gamedata['gHomeGoals']) {
                return true;
            }
        }
        return false;
    }

    private function isWeinstadt($teamName)
    {
        $idObject = $this->fetchIds();
        foreach ($idObject as $team => $teamData) {
            if ($teamName == $team) {
                return true;
            }
        }
        return false;
    }
}