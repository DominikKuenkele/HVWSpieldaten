<?php
/**
 * User: domin
 * Date: 23.05.2018
 * Time: 22:44
 */

include_once('MannschaftsDaten.php');
include_once('SpieldatenLayout.php');

function spieldaten_func($a)
{
    $atts = shortcode_atts(array(
        'team' => 'none',
        'spieldaten' => 'true',
        'tabelle' => 'long',
        'spieltag' => false
    ), $a);

    $mannschaftsDaten = new MannschaftsDaten($atts['team']);
    $spieldatenLayout = new SpieldatenLayout($mannschaftsDaten);

    return $spieldatenLayout->buildLayout($atts['spieldaten'], $atts['tabelle'], $atts['spieltag']);
}