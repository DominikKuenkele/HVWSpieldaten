<?php
/**
 * User: domin
 * Date: 28.05.2018
 * Time: 20:06
 */

foreach ( glob( HVW_SPIELDATEN_ROOT_DIR . "shortcodes/*.php" ) as $file ) {
    include_once $file;
}
foreach ( glob( HVW_SPIELDATEN_ROOT_DIR . "shortcodes/gamesInWeek/*.php" ) as $file ) {
    include_once $file;
}
foreach ( glob( HVW_SPIELDATEN_ROOT_DIR . "shortcodes/spieldaten/*.php" ) as $file ) {
    include_once $file;
}
foreach ( glob( HVW_SPIELDATEN_ROOT_DIR . "html_view/*.php" ) as $file ) {
    include_once $file;
}
foreach ( glob( HVW_SPIELDATEN_ROOT_DIR . "html_view/Chart/*.php" ) as $file ) {
    include_once $file;
}