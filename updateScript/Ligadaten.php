<?php
/**
 * User: Dominik K�nkele
 * Date: 24.10.2017
 * Time: 09:39
 */

include('ICS.php');

class Ligadaten {

    const SPIELDATEN = 'spieldaten';
    const TABELLE = 'tabelle';

    private $columns = array(
        self::SPIELDATEN => array(
            'gDate',
            'gTime',
            'gHomeTeam',
            'gGuestTeam',
            'gHomeGoals',
            'gGuestGoals',
            'gGymnasiumName',
            'gGymnasiumPostal',
            'gGymnasiumTown',
            'gGymnasiumStreet'
        ),
        self::TABELLE    => array(
            'tabScore',
            'tabTeamname',
            'numPlayedGames',
            'numWonGames',
            'numEqualGames',
            'numLostGames',
            'numGoalsShot',
            'numGoalsGot',
            'pointsPlus',
            'pointsMinus'
        )
    );

    const urls = array(
        self::SPIELDATEN => '?cmd=data&lvTypeNext=team&lvIDNext=',
        self::TABELLE    => '?cmd=data&lvTypeNext=class&subType=table&lvIDNext='
    );

    private $ids = array(
        self::SPIELDATEN => 0,
        self::TABELLE    => 0
    );

    private $datenObj = array(
        self::SPIELDATEN => array(),
        self::TABELLE    => array()
    );

    private $teamname = '';
    private $liganame = '';

    /**
     * Ligadaten constructor.
     *
     * @param int $idMannschaft
     * @param int $idLiga
     */
    public function __construct( $idMannschaft = 0, $idLiga = 0 ) {
        $this->ids[ self::SPIELDATEN ] = $idMannschaft;
        $this->ids[ self::TABELLE ]    = $idLiga;

        //fetch team ids, to get the team name
        $idStringJSON = file_get_contents( '../data/ids.json' );
        $idStringObj  = json_decode( $idStringJSON, true );

        foreach ( $idStringObj as $tN => $ligaArray ) {
            foreach ( $ligaArray as $ligaName => $liga ) {
                foreach ( $liga as $type => $id ) {
                    if ( $id == $this->ids[ self::SPIELDATEN ] ) {
                        $this->liganame = $ligaName;
                        $this->teamname = $tN;
                        break 3;
                    }
                }
            }
        }
    }


    /**
     * updates table data for cache
     */
    public function updateTabelle() {
        $this->getSpieldatenFromUrl( self::TABELLE );
    }

    /**
     * updates game data for cache
     */
    public function updateSpieldaten() {
        $this->getSpieldatenFromUrl( self::SPIELDATEN );
    }

    /**
     * updates game data for cache
     */
    public function updateCalendar() {
        if ( $this->liganame <> '' ) {
            if(empty($this->datenObj[self::SPIELDATEN])){
                try {
                    $gameJSON = file_get_contents( '../data/' . $this->ids[self::SPIELDATEN] . '.json');
                    $this->datenObj[self::SPIELDATEN] = json_decode($gameJSON, JSON_PRETTY_PRINT);
                } catch (Exception $exception) {
                    $this->datenObj[self::SPIELDATEN] = array();
                }
            }

            $ics = new ICS( $this->liganame );
            foreach ( $this->datenObj[ self::SPIELDATEN ] as $game ) {

                $starttime   = $this->parseDate($game['gDate'], $game['gTime']);
                $endtime     = $starttime + 60 * 60;
                $name        = $game['gHomeTeam'] . ' - ' . $game['gGuestTeam'];
                $description = 'Spielstand:\n' . $game['gHomeGoals'] . ' - ' . $game['gGuestGoals']
                    . '\n\nOrt:\n'
                    . $game['gGymnasiumName'] . '\n'
                    . $game['gGymnasiumStreet'] . '\n'
                    . $game['gGymnasiumPostal'] . ' ' . $game['gGymnasiumTown'];
                $location    = $game['gGymnasiumName'] . ', '
                    . $game['gGymnasiumStreet'] . ', '
                    . $game['gGymnasiumPostal'] . ' ' . $game['gGymnasiumTown'];
                $ics->add( $starttime, $endtime, $name, $description, $location );
            }
            $ics->save();
        }
    }

    /**
     * @param string $date
     * @param string $time
     *
     * @return int
     */
    private function parseDate($date, $time){
        $dateArray = explode('.', $date);
        $timeArray = explode(':', $time);
        $dateTime = new DateTime();
        $dateTime->setDate('20' . $dateArray[2], $dateArray[1], $dateArray[0]);
        $dateTime->setTime($timeArray[0], $timeArray[1]);
        $dateTime->setTimezone(new DateTimeZone('EUROPE/BERLIN'));

        return $dateTime->getTimestamp();
    }

    /**
     * fetch game data from url
     *
     * @param string $mode
     */
    private function getSpieldatenFromUrl( $mode = '' ) {
        $url  = "https://api.handball4all.de/url/spo_vereine-01.php";
        $url  = $this->fetchFromUrl( $url ) . self::urls[ $mode ] . $this->ids[ $mode ];
        $json = $this->fetchFromUrl( $url );

        //filter data; delete characteristics, which are not in $this->columns array
        $unfilteredObj = json_decode( $json, true );
        $datenObjTemp  = $unfilteredObj[0]['dataList'];

        foreach ( $datenObjTemp as $spiel => $spieldaten ) {
            foreach ( $datenObjTemp[ $spiel ] as $eigenschaft => $value ) {
                if ( in_array( $eigenschaft, $this->columns[ $mode ] ) ) {
                    if ( ( $eigenschaft == 'gHomeTeam' || $eigenschaft == 'gGuestTeam' ) && strpos( $value, 'SG Weinstadt' ) !== false ) {
                        $this->datenObj[ $mode ][ $spiel ][ $eigenschaft ] = $this->teamname;
                    } elseif ( ( $eigenschaft == 'gHomeGoals' || $eigenschaft == 'gGuestGoals' ) && $value == ' ' ) {
                        $this->datenObj[ $mode ][ $spiel ][ $eigenschaft ] = '-';
                    } else {
                        $this->datenObj[ $mode ][ $spiel ][ $eigenschaft ] = $value;
                    }
                }
            }
        }
        if ( $mode == self::SPIELDATEN ) {
            $this->saveSpielDaten();
        }
        if ( $mode == self::TABELLE ) {
            $this->saveTabelleDaten();
        }
    }

    /**
     * save game data to cache
     */
    private function saveSpielDaten() {
        $spieldatenJSON = json_encode( $this->datenObj[ self::SPIELDATEN ] );
        file_put_contents( '../data/' . $this->ids[ self::SPIELDATEN ] . '.json', $spieldatenJSON );
    }

    /**
     * save table data to cache
     */
    private function saveTabelleDaten() {
        $tableJSON = json_encode( $this->datenObj[ self::TABELLE ] );
        file_put_contents( '../data/' . $this->ids[ self::TABELLE ] . '.json', $tableJSON );
    }

    /**
     * get data from url
     *
     * @param $url
     *
     * @return mixed
     */
    private function fetchFromUrl( $url ) {
        $ch = curl_init();
        // Disable SSL verification
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        // Will return the response, if false it print the response
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        // Set the url
        curl_setopt( $ch, CURLOPT_URL, $url );
        // Execute
        $JSON = curl_exec( $ch );
        // Closing
        curl_close( $ch );

        return $JSON;
    }
}
