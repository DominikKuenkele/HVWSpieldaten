<?php
ignore_user_abort(true);
include('Ligadaten.php');

$idStringJSON = file_get_contents('../data/ids.json');
$idStringObj = json_decode($idStringJSON, true);

foreach ($idStringObj as $team) {
    foreach ($team as $spiel){
        $ligadaten = new Ligadaten($spiel['Mannschaft'],$spiel['Liga']);
        $ligadaten->updateTabelle();
        $ligadaten->updateSpieldaten();
        $ligadaten->updateCalendar();
    }
}