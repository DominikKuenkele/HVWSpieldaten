<?php

class ICS
{
    private $data = '';
    private $name = '';
    private $start = "BEGIN:VCALENDAR\nVERSION:2.0\nMETHOD:PUBLISH\n";
    private $end = "END:VCALENDAR\n";

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function add($start, $end, $name, $description, $location)
    {
        $this->data .= "BEGIN:VEVENT\nDTSTART:"
            . date('Ymd\THis\Z', $start) . "\nDTEND:"
            . date('Ymd\THis\Z', $end) . "\nLOCATION:" . $location . "\nTRANSP: OPAQUE\nSEQUENCE:0\nUID:\nDTSTAMP:"
            . date('Ymd\THis\Z') . "\nSUMMARY:" . $name . "\nDESCRIPTION:"
            . $description . "\nPRIORITY:1\nCLASS:PUBLIC\nBEGIN:VALARM\nTRIGGER:-PT10080M\nACTION:DISPLAY\nDESCRIPTION:Reminder\nEND:VALARM\nEND:VEVENT\n";
    }

    public function save()
    {
        file_put_contents('../data/ical/' . $this->name . '.ics', $this->getData());
    }

    public function show()
    {
        header('Content-type:text/calendar');
        header('Content-Disposition: attachment; filename="' . $this->name . '.ics"');
        Header('Content-Length: ' . strlen($this->getData()));
        Header('Connection: close');
        echo $this->getData();
    }

    public function getData()
    {
        return $this->start . $this->data . $this->end;
    }
}
