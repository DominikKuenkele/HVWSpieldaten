<?php
/**
 * User: domin
 * Date: 24.05.2018
 * Time: 19:20
 */

final class Row
{
    private $columns = [];
    private $items;
    private $classes = [];

    final public function __construct(array $columns)
    {
        $this->columns = $columns;
    }

    final public function addItem(string $content, string $column, array $additionalClasses)
    {
        if (array_key_exists($column, $this->columns)) {
            $class = $this->columns[$column];
            foreach ($additionalClasses as $additionalClass) {
                $class .= ' ' . $additionalClass;
            }
            $this->items[$class] = $content;
        } else {
            throw new InvalidArgumentException('Column ' . $column . ' is not defined');
        }
    }

    final public function addClass(string $class)
    {
        $this->classes[] = $class;
    }

    final public function getRow()
    {
        $row = '<tr class="';
        foreach ($this->classes as $class) {
            $row .= ' ' . $class;
        }
        $row .= '">';
        foreach ($this->items as $itemClass => $content) {
            $row .= '<td class="' . $itemClass . '">' . $content . '</td>';
        }
        $row .= '</tr>';

        return $row;
    }
}