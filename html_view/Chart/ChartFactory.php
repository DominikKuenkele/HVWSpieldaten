<?php
/**
 * User: domin
 * Date: 28.05.2018
 * Time: 17:55
 */

final class ChartFactory
{
    private const SHORT_CHART_TABLE_COLUMNS = [
        ''       => 'colScore number',
        'Team'   => 'colTeam',
        'Punkte' => 'colPoints number'
    ];

    private const LONG_CHART_TABLE_COLUMNS = [
        ''       => 'colScore number',
        'Team'   => 'colTeam',
        'Sp'     => 'colPlayedGames number',
        'S'      => 'colWonGames number',
        'U'      => 'colDrawGames number',
        'N'      => 'colLostGames number',
        'Tore'   => 'colGoals number',
        'Punkte' => 'colPoints number'
    ];

    private const CHART_GAMES_COLUMNS = [
        'Dat/Erg' => 'colDatum',
        'Liga' => 'colLiga',
        'Heim'  => 'colHeim',
        'Gast'  => 'colGuest'
    ];

    final public static function getShortChartTableColumns()
    {
        return self::SHORT_CHART_TABLE_COLUMNS;
    }

    final public static function getLongChartTableColumns()
    {
        return self::LONG_CHART_TABLE_COLUMNS;
    }

    final public static function getChartGamesColumns()
    {
        return self::CHART_GAMES_COLUMNS;
    }

    /**
     * @param string $teamName
     * @param bool $forMatchDay
     * @return Chart
     */
    final public function getShortChartTable(string $teamName, bool $forMatchDay)
    {
        $id = $this->normalizeId('chartTable-' . $teamName);
        $class = ['chartTable'];
        if ($forMatchDay){
            $class[] = 'matchDay';
        }
        return new Chart($id, self::SHORT_CHART_TABLE_COLUMNS, $class);
    }

    /**
     * @param string $teamName
     * @param bool $forMatchDay
     * @return Chart
     */
    final public function getLongChartTable(string $teamName, bool $forMatchDay)
    {
        $id = $this->normalizeId('chartTable-' . $teamName);
        $class = ['chartTable'];
        if ($forMatchDay){
            $class[] = 'matchDay';
        }
        return new Chart($id, self::LONG_CHART_TABLE_COLUMNS, $class);
    }

    /**
     * @param string $teamName
     * @param bool $forMatchDay
     * @return Chart
     */
    final public function getChartGames(string $teamName, bool $forMatchDay)
    {
        $id = $this->normalizeId('chartGames-' . $teamName);
        $class = ['chartGames', 'teamPage'];
        if ($forMatchDay){
            $class[] = 'matchDay';
        }
        return new Chart($id, self::CHART_GAMES_COLUMNS, $class);
    }

    /**
     * @param string $week
     * @param bool $forMatchDay
     * @return Chart
     */
    final public function getChartGamesInWeek(string $week, bool $forMatchDay)
    {
        $id = $this->normalizeId('chartGamesInWeek-' . $week);
        $class = ['chartGames'];
        if ($forMatchDay){
            $class[] = 'matchDay';
        }
        return new Chart($id, self::CHART_GAMES_COLUMNS, $class);
    }

    final private function normalizeId(string $id){
        return urlencode($id);
    }
}