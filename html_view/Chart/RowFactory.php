<?php
/**
 * User: domin
 * Date: 28.05.2018
 * Time: 18:20
 */

final class RowFactory
{
    final public function getShortTableRow(){
        $columns = ChartFactory::getShortChartTableColumns();
        return new Row($columns);
    }

    final public function getLongTableRow(){
        $columns = ChartFactory::getLongChartTableColumns();
        return new Row($columns);
    }

    final public function getGamesRow(){
        $columns = ChartFactory::getChartGamesColumns();
        return new Row($columns);
    }
}