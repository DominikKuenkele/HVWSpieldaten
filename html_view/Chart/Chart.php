<?php
/**
 * User: domin
 * Date: 23.10.2017
 * Time: 10:40
 */


final class Chart {

    private $id;
    private $columns;
    private $classes;
    private $tableHead = '';
    private $rows = array();

    /**
     * Chart constructor.
     * @param string $id
     * @param array $columns
     * @param array $classes
     */
    final public function __construct(string $id, array $columns, array $classes)
    {
        $this->id = $id;
        $this->columns = $columns;
        $this->classes = $classes;
        $this->buildHead();
    }

    final private function buildHead(){
        $classString = '';
        foreach ($this->classes as $class){
            $classString .= $class . ' ';
        }
        $this->tableHead = '<table id="' . $this->id . '" class="' . $classString .'"><thead><tr>';
        foreach ($this->columns as $title => $value) {
            $this->tableHead .= '<th id="h' . $title . '">' . $title . '</th>';
        }
        $this->tableHead .= '</tr></thead>';
    }

    /**
     * @param Row $row
     */
    final public function addRow(Row $row) {
        $this->rows[count($this->rows)] = $row->getRow();
    }

    /**
     * @return string
     */
    final public function getId(){
        return $this->id;
    }

    /**
     * @return string
     */
    final public function getTable() {
        if (empty($this->rows)) {
            $table = '';
        } else {
            $table = $this->tableHead;
            $table .= '<tbody>';
            foreach ($this->rows as $row) {
                $table .= $row;
            }
            $table .= '</tbody></table>';
        }
        return $table;
    }
}