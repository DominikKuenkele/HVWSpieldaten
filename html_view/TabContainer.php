<?php
/**
 * User: domin
 * Date: 23.10.2017
 * Time: 09:21
 */

final class TabContainer
{
    private $tabs = array();
    private $tabContainerSCStartTag = '[av_tab_container position="top_tab" boxed="border_tabs" initial="1"]';
    private $tabContainerSCEndTag = '[/av_tab_container]';

    /*
     * [av_tab_container position='top_tab' boxed='border_tabs' initial='1']
     * [av_tab title='Reiter 1' icon_select='no' icon='']
     * Tab Content goes here
     * [/av_tab]
     * [av_tab title='Reiter 2' icon_select='no' icon='']
     * Tab Content goes here
     * [/av_tab]
     * [/av_tab_container]
     */

    final public function addTab($title = '', $content = '')
    {
        $tab = '[av_tab title="' . $title . '" icon_select="no" icon=""]' . $content . '[/av_tab]';
        $this->tabs[count($this->tabs)] = $tab;
    }

    final public function getTabContainer()
    {
        $shortcode = $this->tabContainerSCStartTag;
        foreach ($this->tabs as $tab) {
            $shortcode .= $tab;
        }
        $shortcode .= $this->tabContainerSCEndTag;

        return $shortcode;
    }
}