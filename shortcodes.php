<?php
/**
 * @package HVWSpieldaten
 * @version 1.0
 */

include_once(HVW_SPIELDATEN_ROOT_DIR . 'includes.php');

/**
 * Register style sheet.
 */
function register_plugin_styles() {
    wp_register_style('stylesheet.css', plugin_dir_url(__FILE__) . 'css/stylesheet.css');
    wp_enqueue_style('stylesheet.css');
}
add_action('wp_enqueue_scripts', 'register_plugin_styles');

add_shortcode('spieleDerWoche', 'gamesInWeek_func');
add_shortcode('spieldaten', 'spieldaten_func');
