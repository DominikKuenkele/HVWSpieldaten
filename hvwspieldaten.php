<?php
/*
Plugin Name: HVW spieldaten
Description: Das Plugin holt die spieldaten der Mannschaften von der HVW-Seite und zeigt sie auf der Seite an.
Author: Dominik Künkele
Version: 1.1
*/

if (!defined('ABSPATH')) exit; // Exit if accessed directly

define('HVW_SPIELDATEN_ROOT_DIR', plugin_dir_path(__FILE__));

include("shortcodes.php");
include("admin_area.php");